package vic.qd0.dto;

import javax.annotation.Nonnull;
import java.io.Serializable;

public class UserMessage implements Serializable {

    @Nonnull
    public final User user;

    public UserMessage(@Nonnull User user) {
        this.user = user;
    }

}
