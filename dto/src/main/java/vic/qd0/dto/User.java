package vic.qd0.dto;

import javax.annotation.Nonnull;
import java.util.Objects;

public class User {
    public final int id;

    @Nonnull
    public final String login;

    public User(int id, @Nonnull String login) {
        this.id = id;
        this.login = login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                login.equals(user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                '}';
    }
}
