package vic.qd0;

import com.devexperts.qd.QDContract;
import com.devexperts.qd.QDDistributor;
import com.devexperts.qd.QDFilter;
import com.devexperts.qd.ng.RecordBuffer;
import com.devexperts.qd.ng.RecordCursor;
import com.devexperts.qd.qtp.AgentAdapter;
import com.devexperts.qd.qtp.MessageAdapter;
import com.devexperts.qd.qtp.MessageConnectors;
import com.devexperts.qd.qtp.QDEndpoint;
import com.devexperts.qd.qtp.socket.SocketMessageAdapterFactory;
import com.devexperts.qd.stats.QDStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vic.qd0.dto.User;

import javax.annotation.Nonnull;
import java.net.Socket;
import java.util.Collections;

public class Poster {

    private static final Logger log = LoggerFactory.getLogger(Poster.class);

    private final QDDistributor distributor;
    private final QDEndpoint qdEndpoint;

    private Poster(@Nonnull String distributorAddress) {
        qdEndpoint = QDEndpoint.newBuilder()
                .withName("poster")
                .withScheme(AppScheme.getInstance())
                .withCollectors(Collections.singletonList(QDContract.STREAM))
                .build();
        qdEndpoint.getStream().setEnableWildcards(true);
        distributor = qdEndpoint.getStream().createDistributor(QDFilter.ANYTHING);

        qdEndpoint.addConnectors(MessageConnectors.createMessageConnectors(
                new ServerAdapterFactory(qdEndpoint), distributorAddress, qdEndpoint.getRootStats()
        ));
        qdEndpoint.startConnectors();
    }

    private void start() throws InterruptedException {
        int c = 0;
        while (true) {
            doPost(new User(1000, "login_" + c));
            doPost(new User(1001, "login_" + c));
            doPost(new User(1002, "login_" + c));
            doPost(new User(1003, "login_" + c));
            c += 1;
            Thread.sleep(1000);
        }
    }

    private void doPost(User user) {
        RecordBuffer bf = RecordBuffer.getInstance();
        String symbol = Integer.toString(user.id);
        RecordCursor cursor = bf.add(AppScheme.USER_RECORD, AppScheme.getInstance().getCodec().encode(symbol), symbol);
        cursor.setInt(0, user.id);
        cursor.setObj(0, user.login.getBytes());
        distributor.process(bf);
        bf.release();
    }

    private static class ServerAdapterFactory extends AgentAdapter.Factory
            implements SocketMessageAdapterFactory
    {
        ServerAdapterFactory(QDEndpoint endpoint) {
            super(endpoint, null);
        }

        @Override
        public MessageAdapter createAdapterWithSocket(Socket socket, QDStats stats) throws SecurityException {
            return createAdapter(stats);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new Poster("localhost:10100").start();
    }
}
