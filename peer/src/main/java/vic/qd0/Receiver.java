package vic.qd0;

import com.devexperts.qd.QDAgent;
import com.devexperts.qd.QDContract;
import com.devexperts.qd.QDFilter;
import com.devexperts.qd.ng.*;
import com.devexperts.qd.qtp.DistributorAdapter;
import com.devexperts.qd.qtp.MessageAdapter;
import com.devexperts.qd.qtp.MessageConnectors;
import com.devexperts.qd.qtp.QDEndpoint;
import com.devexperts.qd.qtp.socket.SocketMessageAdapterFactory;
import com.devexperts.qd.stats.QDStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.net.Socket;
import java.util.Collections;
import java.util.concurrent.CountDownLatch;

public class Receiver implements RecordListener {

    private static final Logger log = LoggerFactory.getLogger(Receiver.class);

    private final QDEndpoint qdEndpoint;

    private final QDAgent agent;

    private Receiver(@Nonnull String muxAgentAddress) {
        qdEndpoint = QDEndpoint.newBuilder()
                .withName("receiver")
                .withScheme(AppScheme.getInstance())
                .withCollectors(Collections.singletonList(QDContract.STREAM))
                .build();
        qdEndpoint.getStream().setEnableWildcards(true);

        qdEndpoint.addConnectors(MessageConnectors.createMessageConnectors(
                new ClientAdapterFactory(qdEndpoint), muxAgentAddress, qdEndpoint.getRootStats()
        ));
        qdEndpoint.startConnectors();

        agent = qdEndpoint.getStream().agentBuilder()
                .withFilter(QDFilter.ANYTHING)
                .build();
    }

    private void start() {
        agent.setRecordListener(this);

        AppScheme appScheme = AppScheme.getInstance();
        RecordBuffer sb = RecordBuffer.getInstance(RecordMode.SUBSCRIPTION);
        sb.add(AppScheme.USER_RECORD, appScheme.getCodec().getWildcardCipher(), null);
        agent.setSubscription(sb);
        sb.release();
    }

    @Override
    public void recordsAvailable(RecordProvider provider) {
        log.info("Records available!");

        RecordBuffer buffer = RecordBuffer.getInstance();
        provider.retrieve(buffer);
        RecordCursor cursor;
        while ((cursor = buffer.next()) != null)
            if (cursor.getRecord() == AppScheme.USER_RECORD) {
                int userId = cursor.getInt(0);
                Object userData = cursor.getObj(0);
                log.info("Rcvd {}: {}", userId, new String((byte[])userData));
            }

        buffer.release();
    }

    private static class ClientAdapterFactory extends DistributorAdapter.Factory implements SocketMessageAdapterFactory
    {
        ClientAdapterFactory(QDEndpoint endpoint) {
            super(endpoint, null);
        }

        @Override
        public MessageAdapter createAdapterWithSocket(Socket socket, QDStats stats) throws SecurityException {
            return createAdapter(stats);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new Receiver("localhost:20100").start();
        CountDownLatch latch = new CountDownLatch(1);
        latch.await();
    }
}
