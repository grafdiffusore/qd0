package vic.samples.dxfeed.rmi;

import javax.annotation.Nonnull;

class ServiceImpl implements Service {
    @Nonnull
    @Override
    public String sayHello(@Nonnull String name) {
        return "Hello, " + name + "!";
    }
}
