package vic.samples.dxfeed.rmi;

import com.devexperts.rmi.RMIEndpoint;

import java.util.concurrent.CountDownLatch;

class Server {

    private static final String PLAIN_ONLY_SERVER_ADDRESS = ":7500";

    private static final String SSL_AND_PLAIN_SERVER_ADDRESS = "(:7500)"
            + "(ssl[isServer=true,keyStore=keystore.jks,keyStorePassword=password]+:7501)";

    private final RMIEndpoint endpoint;

    private Server() {
        endpoint = RMIEndpoint.createEndpoint(RMIEndpoint.Side.SERVER);
    }

    private void go() {
        endpoint.getServer().export(new ServiceImpl(), Service.class);
        endpoint.connect(SSL_AND_PLAIN_SERVER_ADDRESS);
    }

    public static void main(String[] args) throws InterruptedException {
        new Server().go();
        new CountDownLatch(1).await();
    }

}
