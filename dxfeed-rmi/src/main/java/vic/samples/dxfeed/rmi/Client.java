package vic.samples.dxfeed.rmi;

import com.devexperts.rmi.RMIEndpoint;
import com.devexperts.rmi.RMIException;

class Client {
    public static void main(String[] args) throws RMIException {
        new Client().go();
    }

    private void go() throws RMIException {
        doRun("localhost:7500");
        doRun("ssl[trustStore=keystore.jks,trustStorePassword=password]+localhost:7501");
    }

    private void doRun(String address) {
        try (RMIEndpoint endpoint = RMIEndpoint.createEndpoint(RMIEndpoint.Side.CLIENT)) {
            endpoint.connect(address);

            Service proxy;
            proxy = endpoint.getClient().getProxy(Service.class);
            System.out.println(">>> result: " + proxy.sayHello("vic"));
        }
    }
}
