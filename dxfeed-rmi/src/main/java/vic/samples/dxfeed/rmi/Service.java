package vic.samples.dxfeed.rmi;

import com.devexperts.rmi.RMIServiceInterface;

import javax.annotation.Nonnull;

@RMIServiceInterface
interface Service {
    @Nonnull
    String sayHello(@Nonnull String name);
}
