package vic.qd0;

import com.devexperts.qd.DataIntField;
import com.devexperts.qd.DataObjField;
import com.devexperts.qd.kit.*;

public class AppScheme extends DefaultScheme {

    static final DefaultRecord USER_RECORD;

    static {
        USER_RECORD = new DefaultRecord(0, "UserMessage", false,
                new DataIntField[]{
                        new PlainIntField(0, "UserMessage.userId"),
                },
                new DataObjField[]{
                        new ByteArrayField(0, "UserMessage.userData")
                }
        );
    }

    private static final AppScheme APP_SCHEME = new AppScheme();

    public static AppScheme getInstance() {
        return APP_SCHEME;
    }

    private AppScheme() {
        super(PentaCodec.INSTANCE, USER_RECORD);
    }

}
